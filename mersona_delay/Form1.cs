﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace mersona_delay
{
    public partial class Form1 : Form
    {
        private int l;
        protected List<double> u_0;
        private double temp_u_k;
        protected List<double>[] u_RunheK;
        protected List<double>[] u_RunheK2;
        protected double N_delay;
        private List<double>[] All_u; 
        private List<double>[] u_k;
        private int n;
        private double H;
        private List<double> x_k;
        private List<double> x;
        private int m;
        private double x_n;
        private double x_0;
        private Func<double,double[]> Func_change;
        //private Func<double, double[]> Prev_Func;
        private List<Func<double, double[]>> Prev_Func;
        public Form1()
        {
            InitializeComponent();
            l = 0;
            temp_u_k = 0;
            label_u_k.Text = "u[ " + l + " ]= ";
            u_0 = new List<double>();
            N_delay = 1;
            n = 1;
            x_k=new List<double>();
            x=new List<double>();
            x_0 = 1;
            x_n = 3;
            H = 0.1;
            IntializeList();
            Func_change = Delay_Func;
        }

        void HelloWorld()
        {
            Console.WriteLine("Hello World!!!");
        }

        void HelloWorld1()
        {
            Console.WriteLine("Hello World1!!!");
        }

        private void IntializeList()
        {
            Prev_Func = new List<Func<double, double[]>>();
            All_u = new List<double>[n];
            u_k = new List<double>[n];
            u_RunheK = new List<double>[n];
            u_RunheK2 = new List<double>[n];
            for (int i = 0; i < n; i++)
            {
                All_u[i] = new List<double>();
                u_k[i] = new List<double>();
                u_RunheK[i] = new List<double>();
                u_RunheK2[i] = new List<double>();
            }
        }

        List<double>[] GetUFirsInterval(double a1 , double b1)
        {
            List<double>[] U=new List<double>[n];
            for (int i = 0; i < n; i++)
            {
                U[i]=new List<double>();
            }
            x.Add(a1);
            while (x[x.Count-1]<=b1+H/2)
            {
                for (int i = 0; i < n; i++)
                {
                    U[i].Add(GetFunctionX(x[x.Count-1])[i]); 
                }
                x.Add(x[x.Count - 1] + H);
            }
            x.RemoveAt(x.Count-1);
            return U;
        }

        private double[] GetFunctionX(double t)
        {
            return new double[]
            {
               0.8
            };
        }

        protected double[] Delay_Func(double t)
        {
            return new double[]
            {
                0.8
            };
        }

        public double[] Function(double[]t,double[] delay)
        {
            double[] mas=new double[delay.Length];
            for (int i = 0; i < delay.Length; i++)
            {
                mas[i] = -1.4*delay[i];
            }
            return mas;
        }

        private double System_Function_Solve(double x)
        { 
            //if (x >= 0 && x <= 1)
            //    return x;
            //if (x > 1 && x <= 2)
            //    return 3*(x - 1)*(x - 1) + 1;
            //else 
            //    return 6*(x - 2)*(x - 2)*(x - 2) + 6*(x - 2) + 4;
            if (x >= -1 && x <= 0)
                return 0.8;
            if (x > 0 && x <= 1)
                return -1.12*x+0.8;
            if (x > 1 && x <= 2)
                return 0.784*(x-1)*(x-1)-1.12*x+0.8;
            if (x > 2 && x <= 3)
                return -0.3659*(x-2)*(x-2)*(x-2)+0.784*(x-1)*(x-1)-1.12*x+0.8;
            else
                return 0.1281 * (x - 3) * (x - 3) * (x - 3) * (x - 3) - 0.3659 * (x - 2) * (x - 2)*(x-2)+0.784*Math.Pow(x-1,2)- 1.12 * x + 0.8;
        }
        protected void Runhe_Kuta_Merson(double _a,double _b)
        {
          
            x_k.Add(_a);
            for (int i = 0; i < n; i++)
            {
                u_RunheK[i].Clear();
                u_RunheK2[i].Clear();
                u_RunheK[i].Add(All_u[i][All_u[i].Count-1]);
                u_RunheK2[i].Add(All_u[i][All_u[i].Count - 1]);
            }
            List<double>[] KValues=new List<double>[n];
            for (int i = 0; i < n; i++)
            {
                KValues[i]=new List<double>();
            }

            while (x_k[x_k.Count - 1] < _b + 0.00001)
            {

                x_k.Add(x_k[x_k.Count - 1] + H);
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values(0, i, KValues));
                }
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values(1, i, KValues));
                }
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values(2, i, KValues));
                }
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values(3, i, KValues));
                }
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values(4, i, KValues));
                }

                for (int i = 0; i < n; i++)
                {
                    u_RunheK[i].Add(u_RunheK[i][u_RunheK[i].Count - 1] + (1 / 2 * H * KValues[i][0]) - (3 / 2 * H * KValues[i][2]) + (2 * H * KValues[i][3]));
                    u_RunheK2[i].Add(u_RunheK[i][u_RunheK[i].Count - 2] + (1 / 6 * H * KValues[i][0]) + (2 / 3 * H * KValues[i][3]) + (1 / 6 * H * KValues[i][4]));

                    KValues[i].Clear();
                }
                
            }
            for (int i = 0; i < n; i++)
            {
                u_RunheK[i].RemoveAt(0);
                u_RunheK2[i].RemoveAt(0);
                u_RunheK[i].RemoveAt(u_RunheK[i].Count - 1);
                u_RunheK2[i].RemoveAt(u_RunheK2[i].Count - 1);
            }
            x_k.RemoveAt(0);
            x_k.RemoveAt(x_k.Count - 1);
            
            for (int i = 0; i < n; i++)
            {
                foreach (var c in u_RunheK[i])
                {
                    All_u[i].Add(c);
                }
            }
            foreach (var c in x_k)
            {
               x.Add(c); 
            }
        }
        protected double Get_k_Values(int k, int k1, List<double>[] KValues1)
        {
            double temp = 0;
            double[] temp_mas_param = new double[n + 1];
            switch (k)
            {
                case 0:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count-1];
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1];
                        }
                        temp = Function(temp_mas_param,Func_change(temp_mas_param[0]))[k1];
                        break;
                    }
                case 1:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count-1] + (double)(H / 3);
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1] + (double)H / 3 * KValues1[i][0];
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }

                case 2:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count-1] + (double)H / 3;
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1] + (double)(H / 6 * KValues1[i][0]) + (double)(H / 6 * KValues1[i][1]);
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }

                case 3:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count-1] + (double)H / 2;
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1] + (double)(H / 8 * KValues1[i][0]) + (double)(3 * H / 8 * KValues1[i][2]);
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }

                case 4:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count-1] + H;
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1] + (double)(H / 2 * KValues1[i][0]) - (double)(3 * H / 2 * KValues1[i][2]) + (double)(2 * H * KValues1[i][3]);
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }

            }
            return temp;
        }

        private double[] Value(int j, double tval)
        {
            double tt0, tt1, tt2, h1, h2, alfa, theta;
            double[] ff0 = new double[n];
            double[] ff1 = new double[n];
            double[] ff2 = new double[n];
            double[] yy0 = new double[n];
            double[] yy1 = new double[n];
            double[] yy2 = new double[n];
            double[] d1 = new double[n];
            double[] d2 = new double[n];
            double[] coef_temp_j = new double[n + 1];
            double[] coef_temp_j_1 = new double[n + 1];
            double[] coef_temp_j_2 = new double[n + 1];
            double[] res = new double[n];
            tt0 = x[j];
            tt1 = x[j + 1];
            tt2 = x[j + 2];
            h1 = tt1 - tt0;
            h2 = tt2 - tt1;
            for (int i = 0; i < n; i++)
            {
                coef_temp_j[0] = x[j];
                coef_temp_j_1[0] = x[j + 1];
                coef_temp_j_2[0] = x[j + 2];
                for (int k = 0; k < n; k++)
                {
                    coef_temp_j[k + 1] = All_u[k][j];
                    coef_temp_j_1[k + 1] = All_u[k][j + 1];
                    coef_temp_j_2[k + 1] = All_u[k][j + 2];
                }
                var prevFunc = ChoosFunc(tval);
                ff0[i] = Function(coef_temp_j, prevFunc(coef_temp_j[0]))[i];
                ff1[i] = Function(coef_temp_j_1, prevFunc(coef_temp_j_1[0]))[i];
                ff2[i] = Function(coef_temp_j_2, prevFunc(coef_temp_j_2[0]))[i];
                yy0[i] = All_u[i][j];
                yy1[i] = All_u[i][j + 1];
                yy2[i] = All_u[i][j + 2];
                d1[i] = (yy1[i] - yy0[i]) / h1;
                d2[i] = (yy1[i] - yy0[i]) / h2;
            }

            alfa = h2 / h1;
            theta = (tval - tt0) / h1;
            for (int i = 0; i < n; i++)
            {
                res[i] = yy0[i] + theta * h1 * (ff0[i] + theta * (d1[i] - ff0[i] + (theta - 1) * (ff1[i] - 2 * d1[i] + ff0[i] + (theta - 1) * (
                    (d2[i] - (1 + alfa) * (1 + alfa) * ff1[i] + alfa * (3 + 2 * alfa) * d1[i] - alfa * (1 + alfa) * ff0[i]) / (alfa * (1 + alfa) * (1 + alfa)) +
                        (theta - 1 - alfa) * ((1 + alfa) * ff2[i] - 2 * (1 + 2 * alfa) * d2[i] + (1 + alfa) * (1 + alfa) * (1 + alfa) * ff1[i] -
                            2 * alfa * alfa * (2 + alfa) * d1[i] + alfa * alfa * (1 + alfa) * ff0[i]) / (alfa * alfa * (1 + alfa) * (1 + alfa) * (1 + alfa))))));
            }
            return res;
        }

        private int FindJ(double val)
        {
            int m = x.Count - 1;
            int j = 0;

            for (int i = 1; i <= m - 1; i++)
                if (val > (x[i - 1] + x[i]) / 2 && val <= (x[i] + x[i + 1]) / 2)
                    j = i - 1;

            if (val >= x[0] && val < x[1])
                j = 0;
            if (val > x[m - 1] && val <= x[m])
                j = m - 2;

            return j;
        }

        private void Mersona_with_Delay()
        {
            for (int i = 0; i < n; i++)
            {
               All_u[i]=GetUFirsInterval(x_0-N_delay,x_0)[i];
                 
            }
            double begin = x_0;
            while (begin<x_n)
            {
                x_k.Clear();
                //Runhe_Kuta_Merson_ChangeH_Ruslan(begin, begin + N_delay);
                Runhe_Kuta_Merson(begin, begin + N_delay);
                Prev_Func.Add( Func_change);
                Func_change = t => Value(FindJ(t - N_delay), t - N_delay);
                begin += N_delay;
            }
           
        }

        Func<double, double[]> ChoosFunc(double t )
        {
            if (Math.Round(x_0-N_delay, 8) <= Math.Round(t, 8) && Math.Round(t, 8) <= Math.Round(x_0 , 8))
                return GetFunctionX;
            
            var i = -1;
            for (var begin = x_0; begin <= x_n; begin += N_delay)
            {
                if (Math.Round(begin, 8) <= Math.Round(t, 8) && Math.Round(t, 8) <= Math.Round(begin + N_delay, 8))
                {
                    i++;
                    return Prev_Func[i];
                }
            }
            return null;
        }

        private void button_result_Click(object sender, EventArgs e)
        {
            Func_change = Delay_Func;
            Prev_Func.Clear();
            for (int i = 0; i < n; i++)
            {
                All_u[i].Clear();
            }
            x.Clear();
            if (m == 0)
                H = 0.1;
            else H = (x_0 + x_n)/m;
            
            Mersona_with_Delay();
            Data_Grid();
            Draw();
            label_Error.Text = Max_Error();
        }

        private void button_u_k_next_Click(object sender, EventArgs e)
        {
            l++;
            label_u_k.Text = "u[ " + l + " ]= ";
            u_0.Add(temp_u_k);
            if (l == n)
            {
                label_u_k.Enabled = false;
                textBox_u_k.Enabled = false;
                button_u_k_next.Enabled = false;
            }
        }
        public void Data_Grid()
        {
            dataGridView.Columns.Clear();
            dataGridView.Columns.Add("Columns1", "i");
            dataGridView.Columns.Add("Columns4", "x[i]");
            for (int i = 0; i < n; i++)
            {
                dataGridView.Columns.Add("Columns" + i, "u" + i + "[i]");
            }
            dataGridView.Rows.Clear();
            for (int i = 0; i < x.Count; i++)
            {
                double[] mas = new double[5];
                for (int j = 0; j < n; j++)
                {
                    mas[j] = All_u[j][i];
                }
                dataGridView.Rows.Add(i, x[i], String.Format("{0:0.00000}", mas[0]), String.Format("{0:0.00000}", mas[1]), String.Format("{0:0.00000}", mas[2]), String.Format("{0:0.00000}", mas[3]), String.Format("{0:0.00000}", mas[4]));
            }
        }

        private void textBox_x0_TextChanged(object sender, EventArgs e)
        {
            if (textBox_x0.Text != "")
            {
                double.TryParse(textBox_x0.Text, out x_0);
            }
        }

        private void textBox_x_n_TextChanged(object sender, EventArgs e)
        {
            if (textBox_x_n.Text != "")
            {
                double.TryParse(textBox_x_n.Text, out x_n);
            }
        }

        private void textBox_N_rozb_TextChanged(object sender, EventArgs e)
        {
            if (textBox_N_rozb.Text != "")
            {
                int.TryParse(textBox_N_rozb.Text, out m);
            }
        }

        private void textBox_n_TextChanged(object sender, EventArgs e)
        {
            if (textBox_n.Text != "")
            {
                int.TryParse(textBox_n.Text, out n);
            }
            IntializeList();
            l = 0;
            u_0.Clear();
            label_u_k.Text = "u[ " + l + " ]= ";
            label_u_k.Enabled = true;
            textBox_u_k.Enabled = true;
            button_u_k_next.Enabled = true;
        }

        private void textBox_u_k_TextChanged(object sender, EventArgs e)
        {
            if (textBox_u_k.Text != "")
            {
                double.TryParse(textBox_u_k.Text, out temp_u_k);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                double.TryParse(textBox1.Text, out N_delay);
            }
        }


        public void Draw()
        {
            double temp_x = x_0 + 0.0001;
            List<GraphPane> myPanes = new List<GraphPane>();
            myPanes.Add(zedGraphControl1.GraphPane);
            myPanes.Add(zedGraphControl2.GraphPane);
            myPanes.Add(zedGraphControl3.GraphPane);
            myPanes.Add(zedGraphControl4.GraphPane);
            for (int i = 0; i < 4; i++)
            {
                myPanes[i].CurveList.Clear();
                myPanes[i].Title.Text = "Graficks";
                myPanes[i].XAxis.Title.Text = "X";
                myPanes[i].YAxis.Title.Text = " Y";
            }

            PointPairList[] listMetod = new PointPairList[n];
            PointPairList[] listFunc = new PointPairList[n];
            for (int i = 0; i < n; i++)
            {
                listFunc[i] = new PointPairList();
                listMetod[i] = new PointPairList();
            }

            for (int i = 0; i < x.Count; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    listMetod[j].Add(x[i], All_u[j][i]);
                }
            }

            while (temp_x <= x_n + 0.001)
            {
                for (int j = 0; j < n; j++)
                {
                    listFunc[j].Add(temp_x, System_Function_Solve(temp_x));
                }
                temp_x += 0.01;
            }
            for (int j = 0; j < n; j++)
            {
                var FunctionCurve2 = myPanes[j].AddCurve("Grafic" + j, listFunc[j], Color.Red, SymbolType.None);
                var FunctionCurve = myPanes[j].AddCurve("Metod" + j, listMetod[j], Color.Blue, SymbolType.Circle);
            }

            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
            zedGraphControl2.AxisChange();
            zedGraphControl2.Invalidate();
            zedGraphControl3.AxisChange();
            zedGraphControl3.Invalidate();
            zedGraphControl4.AxisChange();
            zedGraphControl4.Invalidate();
        }

        string Max_Error()
        {
            string str_error = "MaxError = ";
            double[] error = new double[5];
            List<double> temp = new List<double>();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < All_u[i].Count; j++)
                {
                    temp.Add(Math.Abs(All_u[i][j] - System_Function_Solve(x[j])));
                }
                error[i] = temp.Max();
                str_error = str_error+error[i].ToString()+"\n";
                temp.Clear();
            }
            return str_error;
        }

        protected double Get_k_Values_Ruslan(int k, int k1, List<double>[] KValues1)// k1 number system 
        {
            double temp = 0;
            double[] temp_mas_param = new double[n + 1];
            switch (k)
            {
                case 0:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count - 1];
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1];
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }
                case 1:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count - 1] + (double)(H / 4);
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1] + (double)H / 4 * KValues1[i][0];
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }

                case 2:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count - 1] + (double)3 * H / 8;
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1] + (double)(3 * H / 32 * KValues1[i][0]) + (double)(9 * H / 32 * KValues1[i][1]);
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }

                case 3:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count - 1] + (double)12 * H / 13;
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1] + (double)(1932 * H / 2197 * KValues1[i][0]) - (double)(7200 * H / 2197 * KValues1[i][1]) + (double)(7296 * H / 2197 * KValues1[i][2]);
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }

                case 5:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count - 1] + (double)1 / 2 * H;
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1] - (double)(8 * H / 27 * KValues1[i][0]) + (double)(2 * H * KValues1[i][1]) - (double)(3544 * H / 2565 * KValues1[i][2]) + (double)(1859 * H / 4104 * KValues1[i][3]) - (double)(11 * H / 40 * KValues1[i][4]);
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }
                case 4:
                    {
                        temp_mas_param[0] = x_k[u_RunheK[k1].Count - 1] + H;
                        for (int i = 0; i < n; i++)
                        {
                            temp_mas_param[i + 1] = u_RunheK[i][u_RunheK[i].Count - 1] + (double)(439 * H / 216 * KValues1[i][0]) - (double)(8 * H * KValues1[i][1]) + (double)(3680 * H / 513 * KValues1[i][2]) - (double)(845 * H / 4104 * KValues1[i][3]);
                        }
                        temp = Function(temp_mas_param, Func_change(temp_mas_param[0]))[k1];
                        break;
                    }

            }
            return temp;
        }

        protected void Runhe_Kuta_Merson_ChangeH_Ruslan(double _a,double _b)
        {
            x_k.Add(_a);
            List<double>[] KValues=new List<double>[n];
            KValues[0]=new List<double>();
            for (int i = 0; i < n; i++)
            {
                u_RunheK[i].Clear();
                u_RunheK2[i].Clear();
                u_RunheK[i].Add(All_u[i][All_u[i].Count - 1]);
                u_RunheK2[i].Add(All_u[i][All_u[i].Count - 1]);
            }
            while (x_k[x_k.Count - 1] < _b + 0.00001)
            {
                x_k.Add(x_k[x_k.Count - 1] + H);

                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values_Ruslan(0, i, KValues));
                }
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values_Ruslan(1, i, KValues));
                }
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values_Ruslan(2, i, KValues));
                }
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values_Ruslan(3, i, KValues));
                }
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values_Ruslan(4, i, KValues));
                }
                for (int i = 0; i < n; i++)
                {
                    KValues[i].Add(Get_k_Values_Ruslan(5, i, KValues));
                }

                for (int i = 0; i < n; i++)
                {
                    u_RunheK[i].Add(u_RunheK[i][u_RunheK[i].Count - 1] + ((double) 25/216*H*KValues[i][0]) +
                                    ((double) 1408/2565*H*KValues[i][2]) + ((double) 2197/4104*H*KValues[i][3]) -
                                    ((double) 1/5*H*KValues[i][4]));
                    u_RunheK2[i].Add(u_RunheK[i][u_RunheK2[i].Count - 1] + ((double) 16/135*H*KValues[i][0]) +
                                     ((double) 6656/12825*H*KValues[i][2]) + ((double) 28561/56430*H*KValues[i][3]) -
                                     ((double) 9/50*H*KValues[i][4]) + ((double) 2/55*H*KValues[i][5]));
                    KValues[i].Clear();
                }

                

            }
            for (int i = 0; i < n; i++)
            {
                u_RunheK[i].RemoveAt(0);
                u_RunheK2[i].RemoveAt(0);
                u_RunheK[i].RemoveAt(u_RunheK[i].Count - 1);
                u_RunheK2[i].RemoveAt(u_RunheK2[i].Count - 1);
            }
            x_k.RemoveAt(0);
            x_k.RemoveAt(x_k.Count - 1);

            for (int i = 0; i < n; i++)
            {
                foreach (var c in u_RunheK[i])
                {
                    All_u[i].Add(c);
                }
            }
            foreach (var c in x_k)
            {
                x.Add(c);
            }

        }
    }
}
